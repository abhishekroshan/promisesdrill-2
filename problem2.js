const fs = require("fs").promises;

function readingFile(filePath) {
  return (
    fs
      .readFile(filePath, "utf-8")
      // Read the content of the file
      .then((content) => {
        const upperCaseContent = content.toUpperCase();
        // Converting content to uppercase
        const upperCaseFileName = "upperCaseFile.txt";
        return fs.writeFile(upperCaseFileName, upperCaseContent);
        // Writing the uppercase content to a new file
      })
      .then(() => {
        return fs.appendFile("filenames.txt", `upperCaseFile.txt\n`);
        // Appending the filename to "filenames.txt"
      })
      .then(() => {
        return fs.readFile("upperCaseFile.txt", "utf8");
        // Reading the content of the uppercase file
      })
      .then((upperCaseContent) => {
        const lowerCaseContent = upperCaseContent.toLowerCase();
        // Converting content to lowercase
        const sentence = lowerCaseContent.split(/[.?!]/);
        // Split content into sentences
        const sentenceFileName = "sentenceFile.txt";
        return fs.writeFile(sentenceFileName, sentence.join("\n"));
        // Writing sentences to a new file
      })
      .then(() => {
        return fs.appendFile("filenames.txt", `sentenceFile.txt\n`);
        // Appending the filename to "filenames.txt"
      })
      .then(() => {
        return fs.readFile("sentenceFile.txt", "utf-8");
        // Reading the content of the sentence file
      })
      .then((sentence) => {
        const sortedContent = sentence.split(" ").sort().join("\n");
        // Spliting words, sort, and join them
        const sortedFileName = "sortedFile.txt";
        return fs.writeFile(sortedFileName, sortedContent);
        // Writing sorted content to a new file
      })
      .then(() => {
        return fs.appendFile("filenames.txt", `sortedFile.txt`);
        // Appending the filename to "filenames.txt"
      })
      .then(() => {
        return fs.readFile("filenames.txt", "utf-8");
        // Reading the content of "filenames.txt"
      })
      .then((file) => {
        const filesToDelete = file.split("\n");

        return Promise.all(
          filesToDelete.map((item) => {
            return fs.unlink(item);
            // Deleting files listed in "filenames.txt"
          })
        );
      })
      .catch((error) => {
        console.log(error);
        // Loging any errors that occurred during the process
      })
  );
}

module.exports = readingFile;
