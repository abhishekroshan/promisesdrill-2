const fs = require("fs").promises;

function createAndDeleteFiles(directoryPath, numberOfFiles) {
  return (
    fs
      .mkdir(directoryPath)
      // Create a directory
      .then(() => {
        const promiseArray = [];

        for (let index = 1; index <= numberOfFiles; index++) {
          const fileName = `${directoryPath}/file${index}.json`;
          // Create a unique filename in the specified directory

          const randomData = { value: Math.random() };
          // Generate random data

          const data = JSON.stringify(randomData);
          // Convert data to JSON string

          const writeFilePromise = fs.writeFile(fileName, data);
          // Write data to file
          promiseArray.push(writeFilePromise);
          // Store the promise in an array
        }
        return Promise.all(promiseArray);
        // Wait for all file creation promises to resolve
      })
      .then(() => {
        console.log("All Files Created");

        return fs.readdir(directoryPath);
        // Read the contents of the specified directory
      })
      .then((files) => {
        const deletedFiles = files.forEach((file) => {
          const filePath = `${directoryPath}/${file}`;
          return fs.unlink(filePath);
          // Delete each file in the directory
        });
        console.log("All Files Deleted");
      })
      .catch((error) => {
        console.log(error);
        // Log any errors that occurred during the process
      })
  );
}

module.exports = createAndDeleteFiles;
